
public class App {

	public static void main(String[] args) {

		Car porshe = new Car();
		Car holden = new Car();
		
		System.out.println("Model is " + porshe.getModel());
		porshe.setModel("Carrera");
		System.out.println("Model is " + porshe.getModel());
	}

}
